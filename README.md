# Reproducibility of experiments presented in the paper "Temporal reasoning and query answering with preferences and probabilities for medical decision support"

This guide describes the steps required to reproduce the experimental evaluation presented in the paper:

> Antonella Andolina, Marco Guazzone, Luca Piovesan and Paolo Terenziani.
> *Temporal reasoning and query answering with preferences and probabilities for medical decision support.*
> In Expert Systems With Applications, Volume 195, 2022.
> DOI: [10.1016/j.eswa.2022.116565](https://doi.org/10.1016/j.eswa.2022.116565)

In the above paper, we present a temporal reasoning approach for representing and reasoning with temporal constraints with both preferences and probabilities.
In particular, we define the _Probabilistic+Preferential Quantitative Temporal Network_ (P+PQTN) formalism ans its operations as an extension of the well-known Simple Temporal Problem framework, and we show how it can be integrated in the GLARE-SSCPM system (a computer-interpretable guidelines system) to treat comorbid patients.
We also present the results of a simulation-based experimental evaluation we carried out to assess the performance of our temporal reasoning approach as well as its scalability with respect to the number of time points in a P+PQTN.

To perform the above experimental evaluation, we developed an ad-hoc benchmark program prototype, called `ppqtn_bench`, in C++.
If you use `ppqtn_bench` for your research, please consider citing the above paper.

In this repository, we also publish the artifacts obtained from our experimental evaluation in the hope of fostering research and to provide reproducibility.

**NOTE:** by following this guide, you can reproduce the experimental evaluation we presented in our paper.
However, it is worth noting that the results you will obtain may differ from the ones we published in the paper as they depend on the hardware and software characteristics of the computer where the experiments have been performed (e.g., a computer with a more powerful CPU than the one we used in our experiments will likely yield to lower average execution times).


## Contents of this repository

* `data`: a folder containing the results of the experimental evaluation presented in our paper (see [below](#interpretation-of-results-files) for a discussion about the interpretation of the results files).

* `ppqn_bench-bin_x64-v0.4.1.tar.gz`: the Linux binary package (in TAR.GZ format) of `ppqtn_bench`, the benchmark program prototype we developed to perform the experimental evaluation of our temporal reasoning approach.

* `README.md`: this file

* `scripts`: a folder containing shell, Python and R scripts used to reproduce the experimental evaluation.


## Prerequisites

* **Linux operating system (64 bit).**
We suggest using either [Fedora Linux](https://getfedora.org) version 34 (or higher) or [Ubunti Linux](https://ubuntu.com) version 21.04 (or higher).
The steps described in the next section have been verified on both Fedora Linux 34 and Ubuntu 21.04.


* **Bash shell.**
Our shell scripts require the Bash shell, which is a popular Unix shell that is shipped with most Linux distributions (and for many of them, including the versions of Fedora and Ubuntu suggested above, it is the default login shell).

* **An archiver program that is able to extract TAR.GZ files.**
This program is needed to extract the Linux binary package of `ppqtn_bench`.
In the rest of this guide, we will use the `tar` and `gunzip` commands.


## Additional optional dependencies

The following dependencies are optional and are needed by the scripts used to process the results of the experiments so as to perform curve fitting and create a figure similar to the one we published in Section 5.3 of our paper.
If you are interested only in running the experiments, you can ignore the following dependences.

* **[Python](https://www.python.org) 3.**
We checked our script with Python version 3.9.5 and 3.9.6.
In the rest of this guide, we assume that the Python command-line interpreter is called `python3` (note, in many modern Linux distributions, where only Python 3 is installed, the `python` command is also available as a symbolic link or replacement of the `python3` command).

* **[R](https://www.r-project.org) 4.**
We checked our script with R version 4.0.4 and 4.0.5.
In the rest of this guide, we assume that the command-line R interpreter is called `R` and the front-end program for scripting with R is called `Rscript`.


## Steps for reproducibility

Open a terminal and run the following commands:

1. **Clone this project.**

        git clone https://gitlab.di.unipmn.it/sguazt/ppqtn-eswa2021-artifacts.git

   Note, if you don't have the `git` command installed in your system, you can either install it or download a copy of this repository from this Web site.

2. **Change to the directory where you cloned this project.**

        cd ppqtn-eswa2021-artifacts

3. **Extract the binary package of `ppqtn_beanch`.**

        tar zxvf ppqtn_bench-bin_x64-v0.4.1.tar.gz

4. **Change to the directory where you extracted the above binary package.**

        cd ppqtn-bench-bin_x64

5. **Run the experiments.**

        ./ppqtn_bench-wrapper.sh 10 250 10 5489

   At the end of the execution, the `out` directory will contain several log files storing the raw results (see the [interpretation of results files](#interpretation-of-results-files) section below).

   _NOTE:_ The above command invokes `ppqtn_bench` several times over randomly generated P+PQTNs with a number of time points from 10 to 250 (first and second parameter), with an increasing step of 10 (third parameter), and by setting the seed of the random number generator to 5489 (fourth parameter).
    Specifically, there is one separate invocation for each different number of time points (as defined in the above range); each invocation randomly generates several P+PQTNs (each of which consisting of the given number of time points) by using a random number generator initialized with the seed 5489 (thus assuring that subsequent invocations of `ppqtn_bench` over the same input arguments will always generate the same input P+PQTNs) and until the target relative precision of the 95% confidence interval is reached (see the paper for more details).

6. **Analyze and plot results.** [optional step]

        python3 ../scripts/create_csv.py ./out

        ../scripts/analyze_results.sh ./out/results.csv

   The first command creates a CSV file called `./out/results.csv` containing the execution time statistics extracted from the results generated in the previous step and stored in the `./out` directory.
   The second command invokes the R interpreter and creates a PDF file called `./out/results.pdf`, which represents a plot of data contained in `./out/results.csv` and of polynomial and exponential functions fitted to these data.


## Command-line options of `ppqtn_bench`

The `ppqtn_bench` program is stored in a specific subdirectory of the `ppqtn_bench-bin_x64` directory, which is the main directory created when you extract the binary package by this repository.
To invoke the `ppqtn_bench`, we suggest using the shell script `ppqtn_bench.sh` (also shipped with the binary package), which hides some details abount the direct invocation of the `ppqtn_bench` program.
Whatever of the above method is used to execute `ppqtn_bench`, a single invocation of `ppqtn_bench` will print on the standard output information about the experiment, including the input parameters passed to `ppqtn_bench`, the randomly generated input graphs and various statistics.
We suggest redirecting the standard output of `ppqtn_bench` to a file so as to save the output of an experiment on the persistent storage and to analyze it subsequently.
This is exactly what the script `ppqtn_bench_wrapper.sh` (shipped with the binary package) does.
For instance, the command `./ppqtn_bench-wrapper.sh 10 250 10 5489` (which invokes `ppqtn_bench` several times, one for each considered number of time points in the range from 10 to 250 with an increase step of 10 and by setting to 5489 the seed of the random number generator; see the [steps for reproducibility](#steps-for-reproducibility) section) will save in the directory `out` several files (i.e., `exp-nv_10-seed_5489.log`, `exp-nv_20-seed_5489.log`, ..., `exp-nv_250-seed_5489.log`), one for each considered number of time points.

Both `ppqtn_bench` and `ppqtn_bench.sh` accept the following command-line options:

* **--bench-max-nreps &lt;value&gt;**

  Executes the benchmark at most &lt;value&gt; times (even if the target relative precision has not been reached).

  [default: 18446744073709551615]

* **--bench-min-nreps &lt;value&gt;**

  Executes the benchmark at least &lt;value&gt; times.

  [default: 3]

* **--nverts &lt;value&gt;**

  In a randomly generated graph, sets the number of vertices to &lt;value&gt;.

  [default: 10]

* **--rand-seed &lt;value&gt;**

  Seed for the random number generator.

  [default: 5489]

* **--verbose**

  Enables output verbosity.

  [default: disabled]


## Interpretation of results files

In the following, we describe the output generated by a single invocation of `ppqtn_bench`.
An invocation of `ppqtn_bench` will print on the standard output information about the experiment, including the input parameters passed to `ppqtn_bench`, the randomly generated input graphs and various statistics.
This output consists of three parts:

* The first part contains the version of `ppqtn_bench`, the values of its input arguments (specified through command-line options), and the starting date and time of the execution.
    For instance, consider the following output fragment:

        P+PQTN benchmark v.0.4.1
        -- Options:
        - opt_bench_max_nreps: 18446744073709551615
        - opt_bench_min_nreps: 3
        - opt_help: false
        - opt_nverts: 10
        - opt_rand_seed: 5489
        - opt_verbose: true
        --------------------------------------------------------------------------------
        ****************************************************************
        **** [Tue Aug 24 13:03:25 2021 CEST]
        ****************************************************************

    This fragment specifies that the version of `ppqtn_bench` used is `0.4.1`, the maximum and minimum numbers of runs in an experiment are set to `18446744073709551615` and to 3, respectively (see `opt_bench_max_nreps` and `opt_bench_min_nreps`; note, the value `18446744073709551615` is the maximum integer value representable on the computer where `ppqtn_bench` was run and, in practice, it means no limits), the number of time points is set to 10 (see `opt_nverts`), the seed for the random number generator is set to 5489 (see `opt_rand_seed`), the verbosity of the output messages is enabled (see `opt_verbose`), and finally that the exeuction of `ppqtn_bench` started on August 24 2021 at 13:03:25 CEST.

* The second part contains the input graphs and the statistics computed in each run of the experiment.
    As described in the paper, each experiment (i.e., each invocation of `ppqtn_bench` with a given set of input parameters) performs several runs until the 95% confidence interval of the average execution (CPU) time reaches a relative precision no greater than 4%.
    In particular, in each run, `ppqtn_bench` first randomly generates a new P+PQTN (i.e., a new P+PQTN with the same number of time points as given in input to `ppqtn_bench` but with randomly generated constraints), then applies our temporal reasoning approach, and finally updates the experiment statistics.
    Currently, `ppqtn_bench` collects the following statistics for each run:

    * *CPU time*: the amount of time that our temporal reasoning approach used the CPU to produce the minimal network for an input P+PQTN;
    * *Real time*: the actual time taken by our temporal reasoning approach to produce the minimal network for an input P+PQTN;
    * *Output graph constraint length*: the average length of the constraints of the minimal networks produced by our temporal reasoning approach;
   * *Output graph constraint distance*: the average distance in the constraints of the minimal networks produced by our temporal reasoning approach.

    For each run, `ppqtn_bench` reports in output the randomly generated input graph used by the run, the statistics computed in the end of the run, and the incremental statistics of the experiment (i.e., the aggregate statistics that are incrementally updated after each run).
   For instance, consider the following output fragment:

        **** INPUT GRAPH: digraph  {
        n0 -> n1 [label="<(18, 0.374851, 0.0973682), (19, 0.0324945, 0.226831), (20, 0.140686, 0.0251012), (21, 0.116276, 0.143614), (22, 0.0157724, 0.0934696), (23, 0.0329115, 0.236369), (24, 0.287008, 0.177247)>"]
        n1 -> n2 [label="<(39, %, 0.171667), (40, %, 0.0840018), (41, %, 0.0385183), (42, %, 0.12978), (43, %, 0.105195), (44, %, 0.119174), (45, %, 0.00699866), (46, %, 0.0614681), (47, %, 0.174017), (48, %, 0.109181)>"]
        n2 -> n3 [label="<(4, %, #), (5, %, #), (6, %, #), (7, %, #)>"]
        n3 -> n4 [label="<(90, %, 0.231506), (91, %, 0.235808), (92, %, 0.0860761), (93, %, 0.44661)>"]
        n4 -> n5 [label="<(33, %, 0.162908), (34, %, 0.0012611), (35, %, 0.0853341), (36, %, 0.167066), (37, %, 0.0736412), (38, %, 0.125236), (39, %, 0.148806), (40, %, 0.0522456), (41, %, 0.0578149), (42, %, 0.125686)>"]
        n5 -> n6 [label="<(12, 0.0910012, #), (13, 0.136874, #), (14, 0.169583, #), (15, 0.129129, #), (16, 0.2974, #), (17, 0.139232, #), (18, 0.0367793, #)>"]
        n6 -> n7 [label="<(87, 0.593494, 0.125815), (88, 0.305075, 0.637919), (89, 0.10143, 0.236265)>"]
        n7 -> n8 [label="<(60, %, #), (61, %, #)>"]
        n8 -> n9 [label="<(10, %, #), (11, %, #), (12, %, #), (13, %, #), (14, %, #), (15, %, #)>"]
        }
         
        -- RUN #15 REPORT:
        --- Run statistics:
        * CPU time: 0.00375998
        * Real time: 0.00376497
        * Output graph constraint length: mean: 17.1, s.d.: 12.111, min: 1, max: 45, count: 100
        * Output graph constraint distance: mean: 217.171, s.d.: 104.197, min: 0, max: 397, count: 1710
        --- Incremental benchmark statistics:
        * CPU time - mean: 0.00391755, s.d.: 0.00152108, ci@95.000%: {[0.0030752, 0.00475989], half-width: 0.000842345, relative precision: 0.215019}, min: 0.00124151, max: 0.00684826, count: 15
        * Real time - mean: 0.00392816, s.d.: 0.00152277, ci@95.000%: {[0.00308487, 0.00477144], half-width: 0.000843283, relative precision: 0.214676}, min: 0.00124551, max: 0.00686411, count: 15
        * Output graph average constraint length - mean: 15.8653333, s.d.: 3.32656379, ci@95.000%: {[14.0231437, 17.707523], half-width: 1.84218963, relative precision: 0.116114146}, min: 9.18, max: 21.48, count: 15
        * Output graph average constraint distance - mean: 255.426, s.d.: 52.706949, ci@95.000%: {[226.237865, 284.614135], half-width: 29.1881355, relative precision: 0.114272374}, min: 192.081, max: 335.292, count: 15
        --------------------------------------------------------------------------------


    This fragment contains the input graph in [DOT](https://graphviz.org) format (see `**** INPUT GRAPH: digraph { ... }`), the number of the run (see `-- RUN #15 REPORT:`), the statistics specific to this run (see `--- Run statistics: ...`), and the incremental statistics of the experiment (see `--- Incremental benchmark statistics: ...`); in particular, each incremental statistic provides the following information:

    * *mean*: the mean value (e.g., `mean: 0.00391755`);
    * *s.d.*: the standard deviation (e.g., `s.d.: 0.00152108);;
    * *ci@95.000 {...}*: information about the 95% confidence interval, including its bounds (e.g., `[0.00287342, 0.00287342]`), its half width (e.g., `half-width: 0.000842345`), and its relative precision (e.g., `relative precision: 0.215019`);
    * *min*: the minimum value (e.g., `min: 0.00124151`);
    * *max*: the maximum value (e.g., `max: 0.00684826`);
    * *count*: the number of samples collected (e.g., `count: 15`).

* The third (and final) part contains the summary of the experiment, including the final statistics, the elapsed time since `ppqtn_bench` was started, the ending execution date and time, and finally the same CPU and real time statistics as already reported in the preceding lines.
    For instance, consider the following output fragment:

        -- FINAL REPORT:
        --- Statistics:
        * CPU time - mean: 0.00305686, s.d.: 0.0013571, ci@95.000%: {[0.00293464, 0.00317909], half-width: 0.000122226, relative precision: 0.0399842}, min: 0.000486935, max: 0.00778935, count: 476
        * Real time - mean: 0.00306176, s.d.: 0.00135907, ci@95.000%: {[0.00293936, 0.00318417], half-width: 0.000122404, relative precision: 0.0399781}, min: 0.000488624, max: 0.00779986, count: 476
        * Output graph average constraint length - mean: 15.8466807, s.d.: 3.19922305, ci@95.000%: {[15.5585449, 16.1348164], half-width: 0.288135745, relative precision: 0.0181827192}, min: 6.38, max: 24.6, count: 476
        * Output graph average constraint distance - mean: 258.907859, s.d.: 48.9988789, ci@95.000%: {[254.49481, 263.320908], half-width: 4.41304913, relative precision: 0.0170448635}, min: 125.866, max: 396.813, count: 476
        --------------------------------------------------------------------------------
        **** ELAPSED TIME: 2.08808s
        **** [Tue Aug 24 13:03:27 2021 CEST]
        ****************************************************************
        CPU time: 0.00305686 (s.d. 0.0013571)
        Real time: 0.00306176 (s.d. 0.00135907)


    This fragment contains the final statistics of the experiment (see `--- Statistics:`), the elapsed time in seconds (see `**** ELAPSED TIME: 2.08808s`), the date and time that `ppqtn_bench` ended its execution (in this case, August 24 2021 at 13:03:27 CEST), and finally the final CPU and real time statistics (see `CPU time: ...` and `Real time: ...`, respectively).
