#!/usr/bin/env bash

# Path to the directory containing the scripts used to generated charts
scriptdir=$(dirname $0)
# Path to the output CSV file where to store simulation statitics
datfile=./results.csv
# Output image format
outfmt=pdf
# Fit yes or no?
fit=1

if [ "$#" -gt 0 ]; then
    datfile=$1
fi
if [ "$#" -gt 1 ]; then
    outfmt=$2
fi
if [ "$#" -gt 2 ]; then
    fit=$3
fi

extra_args=""
if [ "$fit" -eq 1 ]; then
    extra_args+=" --fit"
fi

Rscript --vanilla "$scriptdir/analyze_results.R" --datfile "$datfile" --outfmt "$outfmt" --verbose $extra_args
