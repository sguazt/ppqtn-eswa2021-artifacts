# vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4:

from pathlib import Path
import re
import sys


def fatal_error(msg, status = 1):
    print("ERROR: {}.".format(msg))
    sys.exit(status)


def warning(msg):
    print("WARN: {}.".format(msg))


def main(args):

    # Parse arguments

    if len(args) < 1:
        fatal_error("Too few arguments")
    resdir = args[1]
    respath = Path(resdir)
    outfname = args[2] if len(args) > 2 else str(respath.joinpath("results.csv"))

    print("Arguments:")
    print("* Result path: '{}'".format(resdir))
    print("* Output file name: '{}'".format(outfname))

    if not respath.exists():
        fatal_error("Result path not found")

    # Compile regular expresions

    nv_re = re.compile(r'.+-nv_(\d+)-.+$')
    cpu_time_re = re.compile(r'^\s*\*\s+CPU\s+time\s+-\s+mean:\s+(\d+(?:\.\d+)?),\s+s\.d\.:\s+(\d+(?:\.\d+)?),\s+ci@\d+(?:\.\d+)?%:\s+\{\[(\d+(?:\.\d+)?),\s+(\d+(?:\.\d+)?)\],\s+half-width:\s+(\d+(?:\.\d+)?),\s+relative\s+precision:\s+(\d+(?:\.\d+)?)\},\s+min:\s+(\d+(?:\.\d+)?),\s+max:\s+(\d+(?:\.\d+)?),\s+count:\s+(\d+(?:\.\d+)?)\s*$')
    end_mark_re = re.compile(r'^\s*--\s+FINAL\s+REPORT:\s*$')

    # Write CSV output file

    # Retrieve the number of vertices so as  files
    nv_fname_map = {}
    for entry in respath.glob("*.log"):
        if entry.is_file():
            if m := nv_re.match(entry.name):
                nv = int(m.group(1))
                nv_fname_map[nv] = entry
            else:
                warning("Skipped file '{}': unrecognized name structure.".format(str(entry)))

    if len(nv_fname_map) == 0:
        fatal_error("No log file found")

    with open(outfname, 'w') as outfh:
        # Write header
        outfh.write('"nv","mean","sd","ci_lower","ci_upper","count"\n')

        for nv in sorted(nv_fname_map):
            with open(nv_fname_map[nv], 'r') as infh:
                found_end_mark = False
                for line in infh:
                    line = line.strip()

                    if not found_end_mark:
                        if end_mark_re.match(line):
                            found_end_mark = True
                    else:
                        if m := cpu_time_re.match(line):
                            outfh.write('{},{},{},{},{},{}\n'.format(nv, m.group(1), m.group(2), m.group(3), m.group(4), m.group(9)))
                            break
    print("Written file '{}'.".format(outfname))


if __name__ == "__main__":
    main(sys.argv)
